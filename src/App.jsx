import React, { Component } from 'react';
import ResizablePanels from './ResizablePanels';

class App extends Component {
  constructor () {
    super()
    this.state = {
      
    }
  }

  render() {
    return (
      <div>
        <h1>ReactJS Resizable Panels</h1>
        <ResizablePanels>
          <div>
            This is the first panel. It will use the rest of the available space.
          </div>
          <div>
            This is the second panel. Starts with 300px.
          </div>
          <div>
            This is the second panel. Starts with 300px.
          </div>
        </ResizablePanels>
      </div>
    )
  }
}

export default App;
